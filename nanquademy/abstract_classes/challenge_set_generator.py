from abc import ABC, abstractmethod
from typing import Optional, Tuple, List


class ChallengeSetGenerator(ABC):
    @staticmethod
    @abstractmethod
    def generate_challenge() -> Tuple[str, Optional[List[str]], Optional[str], Optional[str]]:
        pass

    @staticmethod
    def render_challenge(challenge_instance: str, path: Optional[str] = None) -> Optional[str]:
        return challenge_instance
