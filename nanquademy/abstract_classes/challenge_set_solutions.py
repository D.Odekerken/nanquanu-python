from abc import ABC, abstractmethod
from typing import List, Tuple, Optional


class ChallengeSetSolutions(ABC):
    @staticmethod
    @abstractmethod
    def generate_solutions(challenge_instance: str, pregenerated_solutions: List[str],
                           solutions_learner: List[str]) -> Tuple[List[str], Optional[str]]:
        pass

    @staticmethod
    def render_solutions(working_dir: str, solutions: List[str]) -> Tuple[List[str], Optional[str]]:
        return solutions, None
