from abc import ABC, abstractmethod
from typing import List, Tuple, Optional


class PerformanceCalculator(ABC):
    @staticmethod
    @abstractmethod
    def local_performance_calculator(challenge_instance: str, solutions_learner: List[str],
                                     pregenerated_solutions: List[str],
                                     verification_info: str) -> Tuple[float, Optional[str]]:
        pass

    @staticmethod
    def performance_calculator(his, solutions_learner: List[str], pregenerated_solutions: List[str],
                               verification_info: str) -> Tuple[float, Optional[str]]:
        return 0, 'Kun je niet altijd berekenen als je geen challenge instance hebt en het volslagen onduidelijk is ' \
                  'wat een his is, dus ik ga deze functie niet implementeren.'
