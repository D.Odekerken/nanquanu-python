from abc import ABC, abstractmethod
from typing import List, Tuple, Optional


class SolutionPrevalidation(ABC):
    @staticmethod
    @abstractmethod
    def prevalidate_solution(challenge_instance: str, solutions_learner: List[str], pregenerated_solutions: List[str],
                             verification_info: str) -> Tuple[str, bool, Optional[str]]:
        pass
