from abc import ABC
from typing import Tuple, Optional, List

from nanquademy.abstract_classes.challenge_set_solutions import ChallengeSetSolutions


class IdentifyGroundedExtensionChallengeSetSolutions(ChallengeSetSolutions, ABC):
    @staticmethod
    def generate_solutions(challenge_instance: str,
                           pregenerated_solutions: List[str],
                           solutions_learner: List[str]) -> Tuple[List[str], Optional[str]]:
        return pregenerated_solutions, None
