from abc import ABC
from typing import Tuple, Optional, List

from py_arg.semantics.get_grounded_extension import get_grounded_extension
from py_arg.generators.abstract_argumentation_framework_generators.abstract_argumentation_framework_generator import \
    AbstractArgumentationFrameworkGenerator
from nanquademy.abstract_classes.challenge_set_generator import ChallengeSetGenerator


class IdentifyGroundedExtensionChallengeSetGenerator(ChallengeSetGenerator, ABC):
    """
    Identify the grounded extension of an abstract argumentation framework.
    """
    @staticmethod
    def generate_challenge() -> Tuple[str, Optional[List[str]], Optional[str], Optional[str]]:
        argumentation_framework_generator = AbstractArgumentationFrameworkGenerator(3, 4, True)
        af = argumentation_framework_generator.generate()

        challenge_instance = ', '.join(argument.name for argument in af.arguments) + ';' + \
                             ', '.join('(' + defeat.from_argument.name + ', ' + defeat.to_argument.name + ')'
                                       for defeat in af.defeats)

        grounded_extension = get_grounded_extension(af)
        grounded_extension_argument_names = [argument.name for argument in grounded_extension]
        verification_info = ','.join(grounded_extension_argument_names)
        return challenge_instance, grounded_extension_argument_names, verification_info, None

    @staticmethod
    def render_challenge(challenge_instance: str, path: Optional[str] = None) -> Optional[str]:
        challenge_instance_parts = challenge_instance.split(';')
        challenge_text = 'Consider an abstract argumentation framework AF = (A, R) with A = [{a}] and R = [{r}]. ' \
                         'Which arguments belong to the grounded extension (divide by commas)?'.format(
            a=challenge_instance_parts[0],
            r=challenge_instance_parts[1])
        return challenge_text
