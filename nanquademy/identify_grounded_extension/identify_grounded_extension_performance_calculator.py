from abc import ABC
from typing import List, Tuple, Optional

from nanquademy.abstract_classes.performance_calculator import PerformanceCalculator


class IdentifyGroundedExtensionPerformanceCalculator(PerformanceCalculator, ABC):
    @staticmethod
    def local_performance_calculator(challenge_instance: str, solutions_learner: List[str],
                                     pregenerated_solutions: List[str],
                                     verification_info: str) -> Tuple[float, Optional[str]]:
        def preprocess(input_str: str) -> List[str]:
            output_unsorted = [solution_part.strip() for solution_part in input_str.upper().strip(',')]
            return sorted(output_unsorted)

        solution_learner = preprocess(solutions_learner[0])
        ground_truth = preprocess(verification_info)
        if solution_learner == ground_truth:
            return 1, None
        else:
            return 0, None
