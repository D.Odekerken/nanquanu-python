from abc import ABC
from typing import List, Tuple, Optional

from nanquademy.abstract_classes.solution_prevalidation import SolutionPrevalidation


class IdentifyGroundedExtensionSolutionPrevalidation(SolutionPrevalidation, ABC):
    @staticmethod
    def prevalidate_solution(challenge_instance: str, solutions_learner: List[str], pregenerated_solutions: List[str],
                             verification_info: str) -> Tuple[str, bool, Optional[str]]:
        return '', True, None
